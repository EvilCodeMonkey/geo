﻿Partial Public Class OSConversion
#Region "OS Conversion"
    Public Shared SemiMajorAxisA As Decimal = 6377563.396 ' Semi-major axis, a
    Public Shared SemiMajorAxisB As Decimal = 6356256.91  ' Semi-major axis, a
    Public Shared TrueOriginEasting As Decimal = 400000.0 + 85  ' True origin Easting, E0	
    Public Shared TrueOriginNorthing As Decimal = -100000.0 - 37  ' True origin Northing, N0	
    Public Shared CentralMeridianScale As Decimal = 0.9996012717   ' Central Meridan Scale, F0	
    Public Shared PHI0 As Decimal = 49.0 ' True origin latitude, j0
    Public Shared LAM0 As Decimal = -2.0 ' True origin longitude, l0
#End Region
End Class
