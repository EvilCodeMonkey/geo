﻿Option Strict Off
Imports System.Math
Partial Public Class OSConversion
    '********************************************************************************************* _
    'THE FUNCTIONS IN THIS MODULE ARE WRITTEN TO BE "STAND ALONE" WITH AS LITTLE DEPENDANCE _
    'ON OTHER FUNCTIONS AS POSSIBLE.  THIS MAKES THEM EASIER TO COPY TO OTHER VB APPLICATIONS. _
    'WHERE A Shared Function DOES CALL ANOTHER FUNCTION THIS IS STATED IN THE COMMENTS AT THE START OF _
    'THE FUNCTION. _
    '*********************************************************************************************

    Shared Function Helmert_X(ByVal X As Decimal, ByVal Y As Decimal, ByVal Z As Decimal, ByVal DX As Decimal, ByVal Y_Rot As Decimal, ByVal Z_Rot As Decimal, ByVal s As Decimal) As Decimal
        'Computed Helmert transformed X coordinate.
        'Input: - _
        'cartesian XYZ coords (X,Y,Z), X translation (DX) all in meters ; _
        'Y and Z rotations in seconds of arc (Y_Rot, Z_Rot) and scale in ppm (s).

        'Convert rotations to radians and ppm scale to a factor
        'Dim PI As Decimal = 3.14159265358979
        Dim sfactor As Decimal = s * 0.000001
        Dim RadY_Rot As Decimal = (Y_Rot / 3600) * (PI / 180)
        Dim RadZ_Rot As Decimal = (Z_Rot / 3600) * (PI / 180)

        'Compute transformed X coord
        Helmert_X = X + (X * sfactor) - (Y * RadZ_Rot) + (Z * RadY_Rot) + DX

    End Function
    Shared Function Helmert_Y(ByVal X As Decimal, ByVal Y As Decimal, ByVal Z As Decimal, ByVal DY As Decimal, ByVal X_Rot As Decimal, ByVal Z_Rot As Decimal, ByVal s As Decimal) As Decimal
        'Computed Helmert transformed Y coordinate.
        'Input: - _
        'cartesian XYZ coords (X,Y,Z), Y translation (DY) all in meters ; _
        'X and Z rotations in seconds of arc (X_Rot, Z_Rot) and scale in ppm (s).

        'Convert rotations to radians and ppm scale to a factor
        'Dim PI As Decimal = 3.14159265358979
        Dim sfactor As Decimal = s * 0.000001
        Dim RadX_Rot As Decimal = (X_Rot / 3600) * (PI / 180)
        Dim RadZ_Rot As Decimal = (Z_Rot / 3600) * (PI / 180)

        'Compute transformed Y coord
        Helmert_Y = (X * RadZ_Rot) + Y + (Y * sfactor) - (Z * RadX_Rot) + DY

    End Function
    Shared Function Helmert_Z(ByVal X As Decimal, ByVal Y As Decimal, ByVal Z As Decimal, ByVal DZ As Decimal, ByVal X_Rot As Decimal, ByVal Y_Rot As Decimal, ByVal s As Decimal) As Decimal
        'Computed Helmert transformed Z coordinate.
        'Input: - _
        'cartesian XYZ coords (X,Y,Z), Z translation (DZ) all in meters ; _
        'X and Y rotations in seconds of arc (X_Rot, Y_Rot) and scale in ppm (s).

        'Convert rotations to radians and ppm scale to a factor
        'Dim PI As Decimal = 3.14159265358979
        Dim sfactor As Decimal = s * 0.000001
        Dim RadX_Rot As Decimal = (X_Rot / 3600) * (PI / 180)
        Dim RadY_Rot As Decimal = (Y_Rot / 3600) * (PI / 180)

        'Compute transformed Z coord
        Helmert_Z = (-1 * X * RadY_Rot) + (Y * RadX_Rot) + Z + (Z * sfactor) + DZ

    End Function
    Shared Function XYZ_to_Lat(ByVal X As Decimal, ByVal Y As Decimal, ByVal Z As Decimal, ByVal a As Decimal, ByVal b As Decimal) As Decimal
        'Convert XYZ to Latitude (PHI) in Dec Degrees.
        'Input: - _
        'XYZ cartesian coords (X,Y,Z) and ellipsoid axis dimensions (a & b), all in meters.

        'THIS Shared Function REQUIRES THE "Iterate_XYZ_to_Lat" FUNCTION
        'THIS Shared Function IS CALLED BY THE "XYZ_to_H" FUNCTION

        Dim RootXYSqr As Decimal = Sqrt((X ^ 2) + (Y ^ 2))
        Dim e2 As Decimal = ((a ^ 2) - (b ^ 2)) / (a ^ 2)
        Dim PHI1 As Decimal = Atan(Z / (RootXYSqr * (1 - e2)))

        Dim PHI As Decimal = Iterate_XYZ_to_Lat(a, e2, PHI1, Z, RootXYSqr)

        'Dim PI As Decimal = 3.14159265358979

        XYZ_to_Lat = PHI * (180 / PI)

    End Function
    Shared Function Iterate_XYZ_to_Lat(ByVal a As Decimal, ByVal e2 As Decimal, ByVal PHI1 As Decimal, ByVal Z As Decimal, ByVal RootXYSqr As Decimal) As Decimal
        'Iteratively computes Latitude (PHI).
        'Input: - _
        'ellipsoid semi major axis (a) in meters; _
        'eta squared (e2); _
        'estimated value for latitude (PHI1) in radians; _
        'cartesian Z coordinate (Z) in meters; _
        'RootXYSqr computed from X & Y in meters.

        'THIS Shared Function IS CALLED BY THE "XYZ_to_PHI" FUNCTION
        'THIS Shared Function IS ALSO USED ON IT'S OWN IN THE _
        '"Projection and Transformation Calculations.xls" SPREADSHEET


        Dim V As Decimal = a / (Sqrt(1 - (e2 * ((Sin(PHI1)) ^ 2))))
        Dim PHI2 As Decimal = Atan((Z + (e2 * V * (Sin(PHI1)))) / RootXYSqr)

        Do While Abs(PHI1 - PHI2) > 0.000000001
            PHI1 = PHI2
            V = a / (Sqrt(1 - (e2 * ((Sin(PHI1)) ^ 2))))
            PHI2 = Atan((Z + (e2 * V * (Sin(PHI1)))) / RootXYSqr)
        Loop

        Iterate_XYZ_to_Lat = PHI2

    End Function
    Shared Function XYZ_to_Long(ByVal X As Decimal, ByVal Y As Decimal) As Decimal
        'Convert XYZ to Longitude (LAM) in Dec Degrees.
        'Input: - _
        'X and Y cartesian coords in meters.

        'Dim PI As Decimal = 3.14159265358979
        XYZ_to_Long = (Atan(Y / X)) * (180 / PI)

    End Function
    Shared Function XYZ_to_H(ByVal X As Decimal, ByVal Y As Decimal, ByVal Z As Decimal, ByVal a As Decimal, ByVal b As Decimal) As Decimal
        'Convert XYZ to Ellipsoidal Height.
        'Input: - _
        'XYZ cartesian coords (X,Y,Z) and ellipsoid axis dimensions (a & b), all in meters.

        'REQUIRES THE "XYZ_to_Lat" FUNCTION

        'Compute PHI (Dec Degrees) first
        Dim PHI As Decimal = XYZ_to_Lat(X, Y, Z, a, b)

        'Convert PHI radians
        'Dim PI As Decimal = 3.14159265358979
        Dim RadPHI As Decimal = PHI * (PI / 180)

        'Compute H
        Dim RootXYSqr As Decimal = Sqrt((X ^ 2) + (Y ^ 2))
        Dim e2 As Decimal = ((a ^ 2) - (b ^ 2)) / (a ^ 2)
        Dim V As Decimal = a / (Sqrt(1 - (e2 * ((Sin(RadPHI)) ^ 2))))
        Dim H As Decimal = (RootXYSqr / Cos(RadPHI)) - V

        XYZ_to_H = H

    End Function
    Shared Function Lat_Long_H_to_X(ByVal PHI As Decimal, ByVal LAM As Decimal, ByVal H As Decimal, ByVal a As Decimal, ByVal b As Decimal) As Decimal
        'Convert geodetic coords lat (PHI), long (LAM) and height (H) to cartesian X coordinate.
        'Input: - _
        'Latitude (PHI)& Longitude (LAM) both in decimal degrees; _
        'Ellipsoidal height (H) and ellipsoid axis dimensions (a & b) all in meters.

        'Convert angle measures to radians
        'Dim PI As Decimal = 3.14159265358979
        Dim RadPHI As Decimal = PHI * (PI / 180)
        Dim RadLAM As Decimal = LAM * (PI / 180)

        'Compute eccentricity squared and nu
        Dim e2 As Decimal = ((a ^ 2) - (b ^ 2)) / (a ^ 2)
        Dim V As Decimal = a / (Sqrt(1 - (e2 * ((Sin(RadPHI)) ^ 2))))

        'Compute X
        Lat_Long_H_to_X = (V + H) * (Cos(RadPHI)) * (Cos(RadLAM))

    End Function
    Shared Function Lat_Long_H_to_Y(ByVal PHI As Decimal, ByVal LAM As Decimal, ByVal H As Decimal, ByVal a As Decimal, ByVal b As Decimal) As Decimal
        'Convert geodetic coords lat (PHI), long (LAM) and height (H) to cartesian Y coordinate.
        'Input: - _
        'Latitude (PHI)& Longitude (LAM) both in decimal degrees; _
        'Ellipsoidal height (H) and ellipsoid axis dimensions (a & b) all in meters.

        'Convert angle measures to radians
        'Dim PI As Decimal = 3.14159265358979
        Dim RadPHI As Decimal = PHI * (PI / 180)
        Dim RadLAM As Decimal = LAM * (PI / 180)

        'Compute eccentricity squared and nu
        Dim e2 As Decimal = ((a ^ 2) - (b ^ 2)) / (a ^ 2)
        Dim V As Decimal = a / (Sqrt(1 - (e2 * ((Sin(RadPHI)) ^ 2))))

        'Compute Y
        Lat_Long_H_to_Y = (V + H) * (Cos(RadPHI)) * (Sin(RadLAM))

    End Function
    Shared Function Lat_H_to_Z(ByVal PHI As Decimal, ByVal H As Decimal, ByVal a As Decimal, ByVal b As Decimal) As Decimal
        'Convert geodetic coord components latitude (PHI) and height (H) to cartesian Z coordinate.
        'Input: - _
        'Latitude (PHI) decimal degrees; _
        'Ellipsoidal height (H) and ellipsoid axis dimensions (a & b) all in meters.

        'Convert angle measures to radians
        'Dim PI As Decimal = 3.14159265358979
        Dim RadPHI As Decimal = PHI * (PI / 180)

        'Compute eccentricity squared and nu
        Dim e2 As Decimal = ((a ^ 2) - (b ^ 2)) / (a ^ 2)
        Dim V As Decimal = a / (Sqrt(1 - (e2 * ((Sin(RadPHI)) ^ 2))))

        'Compute X
        Lat_H_to_Z = ((V * (1 - e2)) + H) * (Sin(RadPHI))

    End Function
    Shared Function Lat_Long_to_East(ByVal PHI As Decimal, ByVal LAM As Decimal, ByVal a As Decimal, ByVal b As Decimal, ByVal e0 As Decimal, ByVal f0 As Decimal, ByVal PHI0 As Decimal, ByVal LAM0 As Decimal) As Decimal
        'Project Latitude and longitude to Transverse Mercator eastings.
        'Input: - _
        'Latitude (PHI) and Longitude (LAM) in decimal degrees; _
        'ellipsoid axis dimensions (a & b) in meters; _
        'eastings of false origin (e0) in meters; _
        'central meridian scale factor (f0); _
        'latitude (PHI0) and longitude (LAM0) of false origin in decimal degrees.

        'Convert angle measures to radians
        'Dim PI As Decimal = 3.14159265358979
        Dim RadPHI As Decimal = PHI * (PI / 180)
        Dim RadLAM As Decimal = LAM * (PI / 180)
        Dim RadPHI0 As Decimal = PHI0 * (PI / 180)
        Dim RadLAM0 As Decimal = LAM0 * (PI / 180)

        Dim af0 As Decimal = a * f0
        Dim bf0 As Decimal = b * f0
        Dim e2 As Decimal = ((af0 ^ 2) - (bf0 ^ 2)) / (af0 ^ 2)
        Dim n As Decimal = (af0 - bf0) / (af0 + bf0)
        Dim nu As Decimal = af0 / (Sqrt(1 - (e2 * ((Sin(RadPHI)) ^ 2))))
        Dim rho As Decimal = (nu * (1 - e2)) / (1 - (e2 * (Sin(RadPHI)) ^ 2))
        Dim eta2 As Decimal = (nu / rho) - 1
        Dim p As Decimal = RadLAM - RadLAM0

        Dim IV As Decimal = nu * (Cos(RadPHI))
        Dim V As Decimal = (nu / 6) * ((Cos(RadPHI)) ^ 3) * ((nu / rho) - ((Tan(RadPHI) ^ 2)))
        Dim VI As Decimal = (nu / 120) * ((Cos(RadPHI)) ^ 5) * (5 - (18 * ((Tan(RadPHI)) ^ 2)) + ((Tan(RadPHI)) ^ 4) + (14 * eta2) - (58 * ((Tan(RadPHI)) ^ 2) * eta2))

        Lat_Long_to_East = e0 + (p * IV) + ((p ^ 3) * V) + ((p ^ 5) * VI)

    End Function
    Shared Function Lat_Long_to_North(ByVal PHI As Decimal, ByVal LAM As Decimal, ByVal a As Decimal, ByVal b As Decimal, ByVal e0 As Decimal, ByVal n0 As Decimal, ByVal f0 As Decimal, ByVal PHI0 As Decimal, ByVal LAM0 As Decimal) As Decimal
        'Project Latitude and longitude to Transverse Mercator northings
        'Input: - _
        'Latitude (PHI) and Longitude (LAM) in decimal degrees; _
        'ellipsoid axis dimensions (a & b) in meters; _
        'eastings (e0) and northings (n0) of false origin in meters; _
        'central meridian scale factor (f0); _
        'latitude (PHI0) and longitude (LAM0) of false origin in decimal degrees.

        'REQUIRES THE "Marc" FUNCTION

        'Convert angle measures to radians
        'Dim PI As Decimal = 3.14159265358979
        Dim RadPHI As Decimal = PHI * (PI / 180)
        Dim RadLAM As Decimal = LAM * (PI / 180)
        Dim RadPHI0 As Decimal = PHI0 * (PI / 180)
        Dim RadLAM0 As Decimal = LAM0 * (PI / 180)

        Dim af0 As Decimal = a * f0
        Dim bf0 As Decimal = b * f0
        Dim e2 As Decimal = ((af0 ^ 2) - (bf0 ^ 2)) / (af0 ^ 2)
        Dim n As Decimal = (af0 - bf0) / (af0 + bf0)
        Dim nu As Decimal = af0 / (Sqrt(1 - (e2 * ((Sin(RadPHI)) ^ 2))))
        Dim rho As Decimal = (nu * (1 - e2)) / (1 - (e2 * (Sin(RadPHI)) ^ 2))
        Dim eta2 As Decimal = (nu / rho) - 1
        Dim p As Decimal = RadLAM - RadLAM0
        Dim M As Decimal = Marc(bf0, n, RadPHI0, RadPHI)

        Dim I As Decimal = M + n0
        Dim II As Decimal = (nu / 2) * (Sin(RadPHI)) * (Cos(RadPHI))
        Dim III As Decimal = ((nu / 24) * (Sin(RadPHI)) * ((Cos(RadPHI)) ^ 3)) * (5 - ((Tan(RadPHI)) ^ 2) + (9 * eta2))
        Dim IIIA As Decimal = ((nu / 720) * (Sin(RadPHI)) * ((Cos(RadPHI)) ^ 5)) * (61 - (58 * ((Tan(RadPHI)) ^ 2)) + ((Tan(RadPHI)) ^ 4))

        Lat_Long_to_North = I + ((p ^ 2) * II) + ((p ^ 4) * III) + ((p ^ 6) * IIIA)

    End Function
    Shared Function E_N_to_Lat(ByVal East As Decimal, ByVal North As Decimal, ByVal a As Decimal, ByVal b As Decimal, ByVal e0 As Decimal, ByVal n0 As Decimal, ByVal f0 As Decimal, ByVal PHI0 As Decimal, ByVal LAM0 As Decimal) As Decimal
        'Un-project Transverse Mercator eastings and northings back to latitude.
        'Input: - _
        'eastings (East) and northings (North) in meters; _
        'ellipsoid axis dimensions (a & b) in meters; _
        'eastings (e0) and northings (n0) of false origin in meters; _
        'central meridian scale factor (f0) and _
        'latitude (PHI0) and longitude (LAM0) of false origin in decimal degrees.

        'REQUIRES THE "Marc" AND "InitialLat" FUNCTIONS

        'Convert angle measures to radians
        'Dim PI As Decimal = 3.14159265358979
        Dim RadPHI0 As Decimal = PHI0 * (PI / 180)
        Dim RadLAM0 As Decimal = LAM0 * (PI / 180)

        'Compute af0, bf0, e squared (e2), n and Et
        Dim af0 As Decimal = a * f0
        Dim bf0 As Decimal = b * f0
        Dim e2 As Decimal = ((af0 ^ 2) - (bf0 ^ 2)) / (af0 ^ 2)
        Dim n As Decimal = (af0 - bf0) / (af0 + bf0)
        Dim Et As Decimal = East - e0

        'Compute initial value for latitude (PHI) in radians
        Dim PHId As Decimal = InitialLat(North, n0, af0, RadPHI0, n, bf0)

        'Compute nu, rho and eta2 using value for PHId
        Dim nu As Decimal = af0 / (Sqrt(1 - (e2 * ((Sin(PHId)) ^ 2))))
        Dim rho As Decimal = (nu * (1 - e2)) / (1 - (e2 * (Sin(PHId)) ^ 2))
        Dim eta2 As Decimal = (nu / rho) - 1

        'Compute Latitude
        Dim VII As Decimal = (Tan(PHId)) / (2 * rho * nu)
        Dim VIII As Decimal = ((Tan(PHId)) / (24 * rho * (nu ^ 3))) * (5 + (3 * ((Tan(PHId)) ^ 2)) + eta2 - (9 * eta2 * ((Tan(PHId)) ^ 2)))
        Dim IX As Decimal = ((Tan(PHId)) / (720 * rho * (nu ^ 5))) * (61 + (90 * ((Tan(PHId)) ^ 2)) + (45 * ((Tan(PHId)) ^ 4)))

        E_N_to_Lat = (180 / PI) * (PHId - ((Et ^ 2) * VII) + ((Et ^ 4) * VIII) - ((Et ^ 6) * IX))

    End Function
    Shared Function E_N_to_Long(ByVal East As Decimal, ByVal North As Decimal, ByVal a As Decimal, ByVal b As Decimal, ByVal e0 As Decimal, ByVal n0 As Decimal, ByVal f0 As Decimal, ByVal PHI0 As Decimal, ByVal LAM0 As Decimal) As Decimal
        'Un-project Transverse Mercator eastings and northings back to longitude.
        'Input: - _
        'eastings (East) and northings (North) in meters; _
        'ellipsoid axis dimensions (a & b) in meters; _
        'eastings (e0) and northings (n0) of false origin in meters; _
        'central meridian scale factor (f0) and _
        'latitude (PHI0) and longitude (LAM0) of false origin in decimal degrees.

        'REQUIRES THE "Marc" AND "InitialLat" FUNCTIONS

        'Convert angle measures to radians
        'Dim PI As Decimal = 3.14159265358979
        Dim RadPHI0 As Decimal = PHI0 * (PI / 180)
        Dim RadLAM0 As Decimal = LAM0 * (PI / 180)

        'Compute af0, bf0, e squared (e2), n and Et
        Dim af0 As Decimal = a * f0
        Dim bf0 As Decimal = b * f0
        Dim e2 As Decimal = ((af0 ^ 2) - (bf0 ^ 2)) / (af0 ^ 2)
        Dim n As Decimal = (af0 - bf0) / (af0 + bf0)
        Dim Et As Decimal = East - e0

        'Compute initial value for latitude (PHI) in radians
        Dim PHId As Decimal = InitialLat(North, n0, af0, RadPHI0, n, bf0)

        'Compute nu, rho and eta2 using value for PHId
        Dim nu As Decimal = af0 / (Sqrt(1 - (e2 * ((Sin(PHId)) ^ 2))))
        Dim rho As Decimal = (nu * (1 - e2)) / (1 - (e2 * (Sin(PHId)) ^ 2))
        Dim eta2 As Decimal = (nu / rho) - 1

        'Compute Longitude
        Dim X As Decimal = ((Cos(PHId)) ^ -1) / nu
        Dim XI As Decimal = (((Cos(PHId)) ^ -1) / (6 * (nu ^ 3))) * ((nu / rho) + (2 * ((Tan(PHId)) ^ 2)))
        Dim XII As Decimal = (((Cos(PHId)) ^ -1) / (120 * (nu ^ 5))) * (5 + (28 * ((Tan(PHId)) ^ 2)) + (24 * ((Tan(PHId)) ^ 4)))
        Dim XIIA As Decimal = (((Cos(PHId)) ^ -1) / (5040 * (nu ^ 7))) * (61 + (662 * ((Tan(PHId)) ^ 2)) + (1320 * ((Tan(PHId)) ^ 4)) + (720 * ((Tan(PHId)) ^ 6)))

        E_N_to_Long = (180 / PI) * (RadLAM0 + (Et * X) - ((Et ^ 3) * XI) + ((Et ^ 5) * XII) - ((Et ^ 7) * XIIA))

    End Function
    Shared Function InitialLat(ByVal North As Decimal, ByVal n0 As Decimal, ByVal afo As Decimal, ByVal PHI0 As Decimal, ByVal n As Decimal, ByVal bfo As Decimal) As Decimal
        'Compute initial value for Latitude (PHI) IN RADIANS.
        'Input: - _
        'northing of point (North) and northing of false origin (n0) in meters; _
        'semi major axis multiplied by central meridian scale factor (af0) in meters; _
        'latitude of false origin (PHI0) IN RADIANS; _
        'n (computed from a, b and f0) and _
        'ellipsoid semi major axis multiplied by central meridian scale factor (bf0) in meters.

        'REQUIRES THE "Marc" FUNCTION
        'THIS Shared Function IS CALLED BY THE "E_N_to_Lat", "E_N_to_Long" and "E_N_to_C" FUNCTIONS
        'THIS Shared Function IS ALSO USED ON IT'S OWN IN THE  "Projection and Transformation Calculations.xls" SPREADSHEET

        'First PHI value (PHI1)
        Dim PHI1 As Decimal = ((North - n0) / afo) + PHI0

        'Calculate M
        Dim M As Decimal = Marc(bfo, n, PHI0, PHI1)

        'Calculate new PHI value (PHI2)
        Dim PHI2 As Decimal = ((North - n0 - M) / afo) + PHI1

        'Iterate to get final value for InitialLat
        Do While Abs(CDec(North - n0 - M)) > 0.00001
            PHI2 = ((North - n0 - M) / afo) + PHI1
            M = Marc(bfo, n, PHI0, PHI2)
            PHI1 = PHI2
        Loop

        InitialLat = PHI2

    End Function
    Shared Function Marc(ByVal bf0 As Decimal, ByVal n As Decimal, ByVal PHI0 As Decimal, ByVal PHI As Decimal) As Decimal
        'Compute meridional arc.
        'Input: - _
        'ellipsoid semi major axis multiplied by central meridian scale factor (bf0) in meters; _
        'n (computed from a, b and f0); _
        'lat of false origin (PHI0) and initial or final latitude of point (PHI) IN RADIANS.

        'THIS Shared Function IS CALLED BY THE - _
        '"Lat_Long_to_North" and "InitialLat" FUNCTIONS
        'THIS Shared Function IS ALSO USED ON IT'S OWN IN THE "Projection and Transformation Calculations.xls" SPREADSHEET

        Marc = bf0 * (((1 + n + ((5 / 4) * (n ^ 2)) + ((5 / 4) * (n ^ 3))) * (PHI - PHI0)) _
        - (((3 * n) + (3 * (n ^ 2)) + ((21 / 8) * (n ^ 3))) * (Sin(PHI - PHI0)) * (Cos(PHI + PHI0))) _
        + ((((15 / 8) * (n ^ 2)) + ((15 / 8) * (n ^ 3))) * (Sin(2 * (PHI - PHI0))) * (Cos(2 * (PHI + PHI0)))) _
        - (((35 / 24) * (n ^ 3)) * (Sin(3 * (PHI - PHI0))) * (Cos(3 * (PHI + PHI0)))))

    End Function
    Shared Function Lat_Long_to_C(ByVal PHI As Decimal, ByVal LAM As Decimal, ByVal LAM0 As Decimal, ByVal a As Decimal, ByVal b As Decimal, ByVal f0 As Decimal) As Decimal
        'Compute convergence (in decimal degrees) from latitude and longitude
        'Input: - _
        'latitude (PHI), longitude (LAM) and longitude (LAM0) of false origin in decimal degrees; _
        'ellipsoid axis dimensions (a & b) in meters; _
        'central meridian scale factor (f0).

        'Convert angle measures to radians
        'Dim PI As Decimal = 3.14159265358979
        Dim RadPHI As Decimal = PHI * (PI / 180)
        Dim RadLAM As Decimal = LAM * (PI / 180)
        Dim RadLAM0 As Decimal = LAM0 * (PI / 180)

        'Compute af0, bf0 and e squared (e2)
        Dim af0 As Decimal = a * f0
        Dim bf0 As Decimal = b * f0
        Dim e2 As Decimal = ((af0 ^ 2) - (bf0 ^ 2)) / (af0 ^ 2)

        'Compute nu, rho, eta2 and p
        Dim nu As Decimal = af0 / (Sqrt(1 - (e2 * ((Sin(RadPHI)) ^ 2))))
        Dim rho As Decimal = (nu * (1 - e2)) / (1 - (e2 * (Sin(RadPHI)) ^ 2))
        Dim eta2 As Decimal = (nu / rho) - 1
        Dim p As Decimal = RadLAM - RadLAM0

        'Compute Convergence
        Dim XIII As Decimal = Sin(RadPHI)
        Dim XIV As Decimal = ((Sin(RadPHI) * ((Cos(RadPHI)) ^ 2)) / 3) * (1 + (3 * eta2) + (2 * (eta2 ^ 2)))
        Dim XV As Decimal = ((Sin(RadPHI) * ((Cos(RadPHI)) ^ 4)) / 15) * (2 - ((Tan(RadPHI)) ^ 2))

        Lat_Long_to_C = (180 / PI) * ((p * XIII) + ((p ^ 3) * XIV) + ((p ^ 5) * XV))

    End Function
    Shared Function E_N_to_C(ByVal East As Decimal, ByVal North As Decimal, ByVal a As Decimal, ByVal b As Decimal, ByVal e0 As Decimal, ByVal n0 As Decimal, ByVal f0 As Decimal, ByVal PHI0 As Decimal) As Decimal
        'Compute convergence (in decimal degrees) from easting and northing
        'Input: - _
        'Eastings (East) and Northings (North) in meters; _
        'ellipsoid axis dimensions (a & b) in meters; _
        'easting (e0) and northing (n0) of true origin in meters; _
        'central meridian scale factor (f0); _
        'latitude of central meridian (PHI0) in decimal degrees.

        'REQUIRES THE "Marc" AND "InitialLat" FUNCTIONS

        'Convert angle measures to radians
        'Dim PI As Decimal = 3.14159265358979
        Dim RadPHI0 = PHI0 * (PI / 180)

        'Compute af0, bf0, e squared (e2), n and Et
        Dim af0 As Decimal = a * f0
        Dim bf0 As Decimal = b * f0
        Dim e2 As Decimal = ((af0 ^ 2) - (bf0 ^ 2)) / (af0 ^ 2)
        Dim n As Decimal = (af0 - bf0) / (af0 + bf0)
        Dim Et As Decimal = East - e0

        'Compute initial value for latitude (PHI) in radians
        Dim PHId As Decimal = InitialLat(North, n0, af0, RadPHI0, n, bf0)

        'Compute nu, rho and eta2 using value for PHId
        Dim nu As Decimal = af0 / (Sqrt(1 - (e2 * ((Sin(PHId)) ^ 2))))
        Dim rho As Decimal = (nu * (1 - e2)) / (1 - (e2 * (Sin(PHId)) ^ 2))
        Dim eta2 As Decimal = (nu / rho) - 1

        'Compute Convergence
        Dim XVI As Decimal = (Tan(PHId)) / nu
        Dim XVII As Decimal = ((Tan(PHId)) / (3 * (nu ^ 3))) * (1 + ((Tan(PHId)) ^ 2) - eta2 - (2 * (eta2 ^ 2)))
        Dim XVIII As Decimal = ((Tan(PHId)) / (15 * (nu ^ 5))) * (2 + (5 * ((Tan(PHId)) ^ 2)) + (3 * ((Tan(PHId)) ^ 4)))

        E_N_to_C = (180 / PI) * ((Et * XVI) - ((Et ^ 3) * XVII) + ((Et ^ 5) * XVIII))

    End Function
    Shared Function Lat_Long_to_LSF(ByVal PHI As Decimal, ByVal LAM As Decimal, ByVal LAM0 As Decimal, ByVal a As Decimal, ByVal b As Decimal, ByVal f0 As Decimal) As Decimal
        'Compute local scale factor from latitude and longitude
        'Input: - _
        'latitude (PHI), longitude (LAM) and longitude (LAM0) of false origin in decimal degrees; _
        'ellipsoid axis dimensions (a & b) in meters; _
        'central meridian scale factor (f0).

        'Convert angle measures to radians
        'Dim PI As Decimal = 3.14159265358979
        Dim RadPHI As Decimal = PHI * (PI / 180)
        Dim RadLAM As Decimal = LAM * (PI / 180)
        Dim RadLAM0 As Decimal = LAM0 * (PI / 180)

        'Compute af0, bf0 and e squared (e2)
        Dim af0 As Decimal = a * f0
        Dim bf0 As Decimal = b * f0
        Dim e2 As Decimal = ((af0 ^ 2) - (bf0 ^ 2)) / (af0 ^ 2)

        'Compute nu, rho, eta2 and p
        Dim nu As Decimal = af0 / (Sqrt(1 - (e2 * ((Sin(RadPHI)) ^ 2))))
        Dim rho As Decimal = (nu * (1 - e2)) / (1 - (e2 * (Sin(RadPHI)) ^ 2))
        Dim eta2 As Decimal = (nu / rho) - 1
        Dim p As Decimal = RadLAM - RadLAM0

        'Compute local scale factor
        Dim XIX As Decimal = ((Cos(RadPHI) ^ 2) / 2) * (1 + eta2)
        Dim XX As Decimal = ((Cos(RadPHI) ^ 4) / 24) * (5 - (4 * ((Tan(RadPHI)) ^ 2)) + (14 * eta2) - (28 * ((Tan(RadPHI * eta2)) ^ 2)))

        Lat_Long_to_LSF = f0 * (1 + ((p ^ 2) * XIX) + ((p ^ 4) * XX))

    End Function
    Shared Function E_N_to_LSF(ByVal East As Decimal, ByVal North As Decimal, ByVal a As Decimal, ByVal b As Decimal, ByVal e0 As Decimal, ByVal n0 As Decimal, ByVal f0 As Decimal, ByVal PHI0 As Decimal) As Decimal
        'Compute local scale factor from from easting and northing
        'Input: - _
        'Eastings (East) and Northings (North) in meters; _
        'ellipsoid axis dimensions (a & b) in meters; _
        'easting (e0) and northing (n0) of true origin in meters; _
        'central meridian scale factor (f0); _
        'latitude of central meridian (PHI0) in decimal degrees.

        'REQUIRES THE "Marc" AND "InitialLat" FUNCTIONS

        'Convert angle measures to radians
        'Dim PI As Decimal = 3.14159265358979
        Dim RadPHI0 As Decimal = PHI0 * (PI / 180)

        'Compute af0, bf0, e squared (e2), n and Et
        Dim af0 As Decimal = a * f0
        Dim bf0 As Decimal = b * f0
        Dim e2 As Decimal = ((af0 ^ 2) - (bf0 ^ 2)) / (af0 ^ 2)
        Dim n As Decimal = (af0 - bf0) / (af0 + bf0)
        Dim Et As Decimal = East - e0

        'Compute initial value for latitude (PHI) in radians
        Dim PHId As Decimal = InitialLat(North, n0, af0, RadPHI0, n, bf0)

        'Compute nu, rho and eta2 using value for PHId
        Dim nu As Decimal = af0 / (Sqrt(1 - (e2 * ((Sin(PHId)) ^ 2))))
        Dim rho As Decimal = (nu * (1 - e2)) / (1 - (e2 * (Sin(PHId)) ^ 2))
        Dim eta2 As Decimal = (nu / rho) - 1

        'Compute local scale factor
        Dim XXI = 1 / (2 * rho * nu)
        Dim XXII = (1 + (4 * eta2)) / (24 * (rho ^ 2) * (nu ^ 2))

        E_N_to_LSF = f0 * (1 + ((Et ^ 2) * XXI) + ((Et ^ 4) * XXII))

    End Function
    Shared Function E_N_to_t_minus_T(ByVal AtEast As Decimal, ByVal AtNorth As Decimal, ByVal ToEast As Decimal, ByVal ToNorth As Decimal, ByVal a As Decimal, ByVal b As Decimal, ByVal e0 As Decimal, ByVal n0 As Decimal, ByVal f0 As Decimal, ByVal PHI0 As Decimal) As Decimal
        'Compute (t-T) correction in decimal degrees at point (AtEast, AtNorth) to point (ToEast,ToNorth)
        'Input: - _
        'Eastings (AtEast) and Northings (AtNorth) in meters, of point where (t-T) is being computed; _
        'Eastings (ToEast) and Northings (ToNorth) in meters, of point at other end of line to which (t-T) is being computed; _
        'ellipsoid axis dimensions (a & b) and easting & northing (e0 & n0) of true origin in meters; _
        'central meridian scale factor (f0); _
        'latitude of central meridian (PHI0) in decimal degrees.

        'REQUIRES THE "Marc" AND "InitialLat" FUNCTIONS

        'Convert angle measures to radians
        'Dim PI As Decimal = 3.14159265358979
        Dim RadPHI0 As Decimal = PHI0 * (PI / 180)

        'Compute af0, bf0, e squared (e2), n and Nm (Northing of mid point)
        Dim af0 As Decimal = a * f0
        Dim bf0 As Decimal = b * f0
        Dim e2 As Decimal = ((af0 ^ 2) - (bf0 ^ 2)) / (af0 ^ 2)
        Dim n As Decimal = (af0 - bf0) / (af0 + bf0)
        Dim Nm As Decimal = (AtNorth + ToNorth) / 2

        'Compute initial value for latitude (PHI) in radians
        Dim PHId As Decimal = InitialLat(Nm, n0, af0, RadPHI0, n, bf0)

        'Compute nu, rho and eta2 using value for PHId
        Dim nu As Decimal = af0 / (Sqrt(1 - (e2 * ((Sin(PHId)) ^ 2))))
        Dim rho As Decimal = (nu * (1 - e2)) / (1 - (e2 * (Sin(PHId)) ^ 2))

        'Compute (t-T)
        Dim XXIII As Decimal = 1 / (6 * nu * rho)

        E_N_to_t_minus_T = (180 / PI) * ((2 * (AtEast - e0)) + (ToEast - e0)) * (AtNorth - ToNorth) * XXIII

    End Function
    Shared Function TrueAzimuth(ByVal AtEast As Decimal, ByVal AtNorth As Decimal, ByVal ToEast As Decimal, ByVal ToNorth As Decimal, ByVal a As Decimal, ByVal b As Decimal, ByVal e0 As Decimal, ByVal n0 As Decimal, ByVal f0 As Decimal, ByVal PHI0 As Decimal) As Decimal
        'Compute true azimuth in decimal degrees at point (AtEast, AtNorth) to point (ToEast,ToNorth)
        'Input: - _
        'Eastings (AtEast) and Northings (AtNorth) in meters, of point where true azimuth is being computed; _
        'Eastings (ToEast) and Northings (ToNorth) in meters, of point at other end of line to which true azimuth is being computed; _
        'ellipsoid axis dimensions (a & b) and easting & northing (e0 & n0) of true origin in meters; _
        'central meridian scale factor (f0); _
        'latitude of central meridian (PHI0) in decimal degrees.

        'REQUIRES THE "Marc", "InitialLat", "t_minus_T" and "E_N_to_C" FUNCTIONS

        'Compute eastings and northings differences
        Dim Diffe As Decimal = ToEast - AtEast
        Dim Diffn As Decimal = ToNorth - AtNorth
        Dim GridBearing
        'Compute grid bearing
        If Diffe = 0 Then
            If Diffn < 0 Then
                GridBearing = 180
            Else
                GridBearing = 0
            End If
            GoTo EndOfComputeBearing
        End If

        Dim Ratio As Decimal = Diffn / Diffe
        'Dim PI As Decimal = 3.14159265358979
        Dim GridAngle As Decimal = (180 / PI) * Atan(Ratio)

        If Diffe > 0 Then
            GridBearing = 90 - GridAngle
        End If

        If Diffe < 0 Then
            GridBearing = 270 - GridAngle
        End If
EndOfComputeBearing:

        'Compute convergence
        Dim Convergence As Decimal = E_N_to_C(AtEast, AtNorth, a, b, e0, n0, f0, PHI0)

        'Compute (t-T) correction
        Dim t_minus_T As Decimal = E_N_to_t_minus_T(AtEast, AtNorth, ToEast, ToNorth, a, b, e0, n0, f0, PHI0)

        'Compute true azimuth
        TrueAzimuth = GridBearing + Convergence - t_minus_T

    End Function

End Class

