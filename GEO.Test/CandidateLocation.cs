﻿namespace Elevate.GEO.Test
{
    public class CandidateLocation
    {
        public CandidateLocation()
        {
            Position = new Position();
        }
        public Position Position { get; set; }
        public int Range { get; set; }
    
    }
}