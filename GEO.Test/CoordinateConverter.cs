﻿using GEO.OS;

namespace Elevate.GEO.Test
{
    public class CoordinateConverter : ICoordinateConverter
    {
        /*decEast = OSConversion.Lat_Long_to_East(decLat, decLong, decSemiMajorAxisA, decSemiMajorAxisB, decTrueOriginEasting, decCentralMeridianScale, DecimalPHI0, DecimalLAM0)                        
         * decNorth = OSConversion.Lat_Long_to_North(decLat, decLong, decSemiMajorAxisA, decSemiMajorAxisB, decTrueOriginEasting, decTrueOriginNorthing, decCentralMeridianScale, DecimalPHI0, DecimalLAM0)
         */

        public OSPosition PositionToOS(Position position)
        {
            var result = new OSPosition
                {
                    Easting =
                        (int) OSConversion.Lat_Long_to_East((decimal) position.Latitude, (decimal) position.Longitude,
                                                            OSConversion.SemiMajorAxisA,
                                                            OSConversion.SemiMajorAxisB,
                                                            OSConversion.TrueOriginEasting,
                                                            OSConversion.CentralMeridianScale,
                                                            OSConversion.PHI0,
                                                            OSConversion.LAM0),
                    Northing =
                        (int) OSConversion.Lat_Long_to_North((decimal) position.Latitude, (decimal) position.Longitude,
                                                             OSConversion.SemiMajorAxisA,
                                                             OSConversion.SemiMajorAxisB,
                                                             OSConversion.TrueOriginEasting,
                                                             OSConversion.TrueOriginNorthing,
                                                             OSConversion.CentralMeridianScale,
                                                             OSConversion.PHI0,
                                                             OSConversion.LAM0)
                };
            return result;
        }
                        
        public Position OSToPosition(OSPosition position)
        {
            var result = new Position
                {
                    Latitude =
                        (double)
                        OSConversion.E_N_to_Lat(position.Easting, position.Northing, OSConversion.SemiMajorAxisA,
                                                OSConversion.SemiMajorAxisB,
                                                OSConversion.TrueOriginEasting,
                                                OSConversion.TrueOriginNorthing,
                                                OSConversion.CentralMeridianScale,
                                                OSConversion.PHI0,
                                                OSConversion.LAM0),
                    Longitude = (double) OSConversion.E_N_to_Long(position.Easting, position.Northing,
                                                                  OSConversion.SemiMajorAxisA,
                                                                  OSConversion.SemiMajorAxisB,
                                                                  OSConversion.TrueOriginEasting,
                                                                  OSConversion.TrueOriginNorthing,
                                                                  OSConversion.CentralMeridianScale,
                                                                  OSConversion.PHI0,
                                                                  OSConversion.LAM0)
                };

            return result;
        }
    }
}