﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Elevate.GEO.Test
{
    public class GeoMatch
    {

        private readonly ICoordinateConverter _coordinatreConverter;

        public GeoMatch()
        {
            _coordinatreConverter = new CoordinateConverter();
        }
        
        
        /// <summary>
        /// Establishes a list of Jobs that match the skills as those that are within the range of any of the specified areas (position with range)
        /// </summary>
        /// <param name="locations">The candidates preferred locations</param>
        /// <param name="matchingJobs">The jobs matching the candidates skills</param>
        /// <returns></returns>
        public IEnumerable<Job> FindJobsInRange(IEnumerable<CandidateLocation> locations, IEnumerable<Job> matchingJobs)
        {
            var result = new HashSet<Job>();
            foreach (var location in locations)
            {
                if (location.Position.OS == null)
                {
                    location.Position.OS = _coordinatreConverter.PositionToOS(location.Position);
                }
                foreach (var matchingJob in matchingJobs)
                {
                    if (matchingJob.Position.OS == null)
                    {
                        matchingJob.Position.OS = _coordinatreConverter.PositionToOS(matchingJob.Position);
                    }

                    var east = Math.Abs(location.Position.OS.Easting - matchingJob.Position.OS.Easting);
                    var north = Math.Abs(location.Position.OS.Northing - matchingJob.Position.OS.Northing);

                    var distance = CalcualteDistance(east, north);
                    Trace.WriteLine(String.Format("Client {0} is distance of {1} matched against a range of {2}", matchingJob.Client, distance, location.Range));
                    if (distance < location.Range) result.Add(matchingJob);
                }
            }
            return result;
        }

        public double CalcualteDistance(decimal east, decimal north)
        {
            decimal eastSquared = (east /1000)  * (east/1000);
            decimal northSquared = (north/1000) * (north/1000);

            decimal hypo = eastSquared + northSquared;

            var distance = Math.Sqrt((double)hypo) * 1000;
            return distance;
        }
    }
}