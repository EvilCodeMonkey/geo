﻿namespace Elevate.GEO.Test
{
    public interface ICoordinateConverter
    {

        OSPosition PositionToOS(Position position);
        Position OSToPosition(OSPosition position);
    }
}