﻿namespace Elevate.GEO.Test
{
    public class Job
    {
        public Job()
        {
            Position = new Position();
        }
        public string Client { get; set; }
        public Position Position { get; set; }
       
    }
}