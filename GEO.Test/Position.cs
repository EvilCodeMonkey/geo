﻿namespace Elevate.GEO.Test
{
    public class Position
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public OSPosition OS { get; set; }
    }
}