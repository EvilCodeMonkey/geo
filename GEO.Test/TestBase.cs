﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace Elevate.GEO.Test
{
    [TestFixture]
    public abstract class TestBase<T>
    {
        protected T _unitUnderTest;
     
        
        [SetUp]
        public void Setup()
        {
            GivenThat();
            When();
        }

        protected virtual void When()
        {
            
        }

        protected virtual void GivenThat()
        {
            _unitUnderTest = Activator.CreateInstance<T>();
        }
    }
}