﻿namespace Elevate.GEO.Test
{
    public abstract class TestCoordindateBase: TestBase<CoordinateConverter>
    {
        protected Position Position;
        protected OSPosition OSPosition;
        protected const double LATITUDE = 52.04059d;
        protected const double LONGITUDE = -4.0427d;
        protected const int EASTING = 260000;
        protected const int NORTHING = 240000;

    }
}