﻿using System;
using NUnit.Framework;

namespace Elevate.GEO.Test
{
    public class TestCoordindateConversionOSToPosition : TestCoordindateBase
    {
        protected override void GivenThat()
        {
            base.GivenThat();
            OSPosition = new OSPosition() { Easting = EASTING, Northing = NORTHING };
            
        }
        protected override void When()
        {
            base.When();
            Position = _unitUnderTest.OSToPosition(OSPosition);
        }

        [Test]
        public void ItShouldHaveTheCorrectPosition()
        {
            Assert.AreEqual(Math.Round(LATITUDE, 5), Math.Round(Position.Latitude, 5));
            Assert.AreEqual(Math.Round(LONGITUDE, 5), Math.Round(Position.Longitude, 5));
        }

    }

    

}