﻿using NUnit.Framework;

namespace Elevate.GEO.Test
{
    public class TestCoordindateConversionPositionToOS : TestCoordindateBase
    {
        protected override void GivenThat()
        {
            base.GivenThat();
            Position = new Position() { Latitude = LATITUDE, Longitude = LONGITUDE };
            
        }
        protected override void When()
        {
            base.When();
            Position.OS = _unitUnderTest.PositionToOS(Position);
        }

        [Test]
        public void ItShouldHaveTheCorrectOS()
        {
            Assert.AreEqual(EASTING, Position.OS.Easting);
            Assert.AreEqual(NORTHING, Position.OS.Northing);
        }

    }
}