﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Elevate.GEO.Test
{
    [TestFixture]
    public class TestGeoMatch : TestGeoMatchBase
    {
        private double ncLat = 51.450340 ;
        private double ncLon = -2.585626;
        private double rcLat = 51.238637 ;
        private double rcLon = -0.583689;


        protected override void GivenThat()
        {
            base.GivenThat();
            range = 150000;
            _candidateLocations = new HashSet<CandidateLocation>
                {
                    new CandidateLocation
                        {
                            Position = new Position{Latitude = LATITUDE,Longitude = LONGITUDE,},
                            Range = range
                        }
                };

            var position = new Position { Latitude = ncLat, Longitude = ncLon };
            var position2 = new Position { Latitude = rcLat, Longitude = rcLon };
            var job = new Job { Client = "Near Client", Position = position };
            var job2 = new Job { Client = "Remote Client", Position = position2 };
            _jobsMatchingSkills = new HashSet<Job> { job, job2 };
            _expectedNumberOfMatchingJobs = 1;
        }

        protected override void When()
        {
            _actualNumberOfMatchingJobs =
                _unitUnderTest.FindJobsInRange(_candidateLocations, _jobsMatchingSkills).Count();
        }

        
        [Test]
        public void ItShouldIncludeOnlyJobsWithAcceptedRanges()
        {
            Assert.AreEqual(_expectedNumberOfMatchingJobs, _actualNumberOfMatchingJobs);
        }
    }
}