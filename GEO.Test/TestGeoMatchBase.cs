﻿using System.Collections.Generic;

namespace Elevate.GEO.Test
{
    public abstract class TestGeoMatchBase: TestBase<GeoMatch>
    {
        protected const double LATITUDE = 52.04059d;
        protected const double LONGITUDE = -4.0427d;
        protected const int EASTING = 260000;
        protected const int NORTHING = 240000;
        protected int range = 150000; 
        protected int inRange = 100000;
        protected int outOfRange = 175000;
        protected HashSet<Job> _jobsMatchingSkills;
        protected HashSet<CandidateLocation> _candidateLocations;
        protected int _actualNumberOfMatchingJobs;
        protected int _expectedNumberOfMatchingJobs;

    }
}