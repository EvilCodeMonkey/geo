﻿using System;
using NUnit.Framework;

namespace Elevate.GEO.Test
{
    [TestFixture]
    public class TestGeoMatchDistance: TestGeoMatchBase
    {
        private double _result;
        private double _expected;
        private decimal _east;
        private decimal _north;
        private const int dp = 2;
        protected override void GivenThat()
        {
            base.GivenThat();

            _east = 800000;
            _north = 800000;
            _expected = 1131370.85d;

        }
            
        protected override void When()
        {
            base.When();
            _result = _unitUnderTest.CalcualteDistance(_east, _north);
            
        }

        [Test]
        public void ItShouldReturnTheCorrectDistance()
        {
            Assert.AreEqual(Math.Round(_expected,dp), Math.Round(_result,dp));
        }
    }
}