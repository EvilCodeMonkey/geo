﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Elevate.GEO.Test
{
    [TestFixture]
    public class TestOsBasedGeoMatch : TestGeoMatchBase
    {
        protected HashSet<Job> _jobsMatchingSkills;
        protected HashSet<CandidateLocation> _candidateLocations;
        protected int _actualNumberOfMatchingJobs;
        protected int _expectedNumberOfMatchingJobs;

        protected override void GivenThat()
        {
            base.GivenThat();
            _candidateLocations = new HashSet<CandidateLocation>
                {
                    new CandidateLocation
                        {
                            Position = new Position(){OS = new OSPosition{Easting = EASTING,Northing = NORTHING,}},
                                Range = range
                            
                        }
                };


            var position = new Position { OS = new OSPosition { Easting = EASTING - inRange, Northing = NORTHING + inRange } };
            var position2 = new Position { OS = new OSPosition { Easting = EASTING + outOfRange, Northing = NORTHING + outOfRange } };
            var job1 = new Job { Client = "Near Client", Position = position };
            var job2 = new Job { Client = "Remote Client", Position = position2 };
            _jobsMatchingSkills = new HashSet<Job> { job1, job2 };
            _expectedNumberOfMatchingJobs = 1;

        }

        protected override void When()
        {
            
            _actualNumberOfMatchingJobs = _unitUnderTest.FindJobsInRange(_candidateLocations, _jobsMatchingSkills).Count();

        }

        
        [Test]
        public void ItShouldIncludeOnlyJobsWithAcceptedRanges()
        {
            _expectedNumberOfMatchingJobs = 1;

            Assert.AreEqual(_expectedNumberOfMatchingJobs, _actualNumberOfMatchingJobs);
        }
    }
}